<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package cw
 */

?>

<footer>
	
	<div class="container">

		<div class="row">

			<div class="col-sm-12" style="text-align:center;">
				
				<p class="title--medium title--center title--white title--push-large fadeInUp">Visit our website<br><a href="http://www.leicestercp.com" target="_blank">www.leicestercp.com</a></p>

			</div>

		</div>

		<div class="row">

			<div class="col-sm-12" style="text-align:center;">
				<a href="http://www.matherjamie.co.uk/" target="_blank"><img src="<?php bloginfo('template_url'); ?>/img/mather-jamie-logo.jpg" alt="Mather Jamie logo" class="footer-logo fadeInUp" /></a>
				<a href="http://www.avisonyoung.com/en_CA" target="_blank"><img src="<?php bloginfo('template_url'); ?>/img/avison-young-logo.jpg" alt="Avison Young logo" class="footer-logo fadeInUp" /></a>
				<a href="http://www.burbagerealty.com/" target="_blank"><img src="<?php bloginfo('template_url'); ?>/img/burbage-realty-logo.jpg" alt="Burbage Realty logo" class="footer-logo fadeInUp" /></a>
			</div>

		</div>

		<div class="row">

			<div class="col-sm-12" style="text-align:center;">

				<a href="http://www.wilsonbowden.co.uk/" target="_blank"><img src="<?php bloginfo('template_url'); ?>/img/wilson-bowden-logo.png" alt="Wilson Bowden logo" class="footer-logo-2 fadeInUp" /></a>
				
				<div class="line fadeInUp"></div>
			
			</div>

		</div>

		<div class="row">

			<div class="col-sm-12 col-md-6">

				<p class="fadeInUp">&copy; Copyright Goodman 2018. <span class="tc">Terms and conditions</span> apply</p>

			</div>

			<div class="col-sm-12 col-md-6">
				<div class="fadeInUp">

					<a href="https://twitter.com/Goodman_group" class="social social--twitter" target="_blank"><i class="fa fa-twitter"></i></a>
					<h4>Follow Us</h4>
					<a href="https://www.linkedin.com/company/goodman" class="social social--linkedin" target="_blank"><i class="fa fa-linkedin"></i></a>
					<h4>Connect</h4>

				</div>
			</div>

		</div>

	</div>

</footer>

<!-- Jquery -->
<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>

<!-- Bootstrap -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<!-- Lightbox -->
<script src="<?php bloginfo('template_url'); ?>/js/featherlight.min.js"></script>

<!-- Instagram -->
<script src="<?php bloginfo('template_url'); ?>/js/instafeed.js"></script>

<!-- Animation Libraries -->
<script src="<?php bloginfo('template_url'); ?>/js/TweenMax.min.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/ScrollMagic.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/animation.gsap.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/debug.addIndicators.js"></script>

<!-- Scroll Bar -->
<script src="<?php bloginfo('template_url'); ?>/js/jquery.mCustomScrollbar.concat.min.js"></script>
        


<!-- Custom JS -->
<script src="<?php bloginfo('template_url'); ?>/js/custom.js"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-116001343-1"></script>

<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-116001343-1');
</script>


<?php wp_footer(); ?>

</body>
</html>
