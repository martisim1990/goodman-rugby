<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package cw
 */

get_header(); ?>

	<div id="primary" class="content-area">

		<main id="main" class="site-main">

			<section class="section">

				<div class="container">

					<div class="row">

						<div class="col-sm-12">

							<h2 class="title title--white title--xlarge title--center title--main fadeInTop">404<span class="title--green title--normal">+</span></h2>

							<h2 class="title title--medium title--white title--main fadeInTop">The page you are looking for cannot be found</h2>

							<img src="<?php bloginfo('template_url'); ?>/img/header-goodman.jpg" alt="Win Premiership final VIP tickets" class="rugby fadeInTop" />

						</div>

					</div>

					<div class="line fadeInTop"></div>

				</div>

			</section>

		</main>

	</div>

<?php
get_footer();
