<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package cw
 */

get_header(); ?>

	<div id="primary" class="content-area">

		<main id="main" class="site-main">

			<section class="section">

				<div class="container">

					<div class="row">

						<div class="col-sm-12">

							<h2 class="title title--white title--xlarge title--center title--main fadeInTop">PERFECTLY PLACED<br>TO WIN A GREAT PRIZE<span class="title--green title--normal">+</span></h2>

							<h2 class="title title--medium title--green title--main fadeInTop">Leicester Commercial Park <span class="title--white title--light">M1(J21) &amp; M69(J3)</span></h2>

							<img src="<?php bloginfo('template_url'); ?>/img/header-goodman.jpg" alt="Win Premiership final VIP tickets" class="rugby fadeInTop" />

							<h2 class="title title--white title--medium title--center fadeInTop">
								<span style="display:inline-block;margin-bottom:5px;">Play our Spot the Ball game and convert your try into</span><br>
								<span class="title--green title--upper" style="display:inline-block;margin-bottom:5px;">VIP AVIVA PREMIERSHIP RUGBY FINAL TICKETS<br>For Four with Hospitality</span><br>
								<span style="display:inline-block;margin-bottom:5px;">on the 26th May 2018 at Twickenham</span><br>
								<span class="title--light" style="max-width:800px;margin:0 auto;display:inline-block;">Plus, three runners up will each receive a signed rugby shirt and a signed rugby ball
								<span class="title--green title--normal">+</span></span>
							</h2>

							<img src="<?php bloginfo('template_url'); ?>/img/ball.png" alt="Rugby Balls" class="ball fadeInTop" />

							<p class="fadeInTop">Get the VIP treatment at the home of rugby on one of the biggest days in the sporting calendar. Or win one of our exclusive signed rugby shirt and ball runner-up prizes. Just play Spot the Ball.*</p>

						</div>

					</div>

					<div class="line fadeInTop"></div>

				</div>

			</section>

			<section class="section">

				<div class="container">

					<div class="row">

						<div class="col-sm-12">

							<h2 class="title title--large title--normal title--white fadeInTop">Enter the perfect position for the ball<span class="title--green title--normal">+</span></h2>

							<p class="fadeInTop">Here’s our classic line-out. It has a receiver, a thrower, a ref, a non-throwing hooker... but no ball.<br>Guess where it is and submit your answer below. Good luck!</p>

							<img src="<?php bloginfo('template_url'); ?>/img/spot.png" alt="Spot the ball" class="spot fadeInTop" />

							<h2 class="title title--medium title--normal title--green fadeInTop">Enter your co-ordinates below</h2>

							<div class="fadeInTop">
							
								<?php echo do_shortcode('[contact-form-7 id="46" title="Contact form 1"]'); ?>
							
							</div>

							<p class="small fadeInTop">*Please see competition <a href="#" class="tc" style="color:#fff!important;">Terms and Conditions</a> below.<br>Winners will be notified via email.</p>
					
						</div>

					</div>

					<div class="line fadeInTop"></div>

				</div>

			</section>

			<section class="section">

				<div class="container">
				
					<div class="row">

						<div class="col-sm-12">

							<h2 class="title title--large title--white fadeInTop">PERFECTLY PLACED<span class="title--green title--normal">+</span></h2>

							<h2 class="title title--medium title--green fadeInTop">Leicester Commercial Park <span class="title--white title--light">M1(J21) &amp; M69(J3)</span></h2>

							<p class="push fadeInTop">Located in one of the most sought after logistics locations in the UK, Leicester Commercial Park is ideally placed to serve the needs of industrial/distribution occupiers.</p>

							<img src="<?php bloginfo('template_url'); ?>/img/aerial.jpg" alt="Goodman aerial map" class="aerial fadeInTop" />

						</div>

					</div>

				</div>

			</section>

			

			<section class="section">

				<div class="container">

					<div class="row">
					
						<div class="col-sm-12 col-md-6 border-right mobile-push">

							<h2 class="title title--medium title--green fadeInTop">Leicester 335</h2>

							<img src="<?php bloginfo('template_url'); ?>/img/leicester-335.jpg" alt="Leicester 335 Image" class="leicester fadeInTop" />

							<img src="<?php bloginfo('template_url'); ?>/img/warehouse.png" alt="Warehous icon" class="warehouse fadeInTop" />

							<div class="warehouse-wrapper">

								<h2 class="title title--large title--green fadeInTop">335,000<span class="title--small">sq ft</span></h2>

								<h2 class="title title--small title--white fadeInTop">High quality, logistics and<br>warehouse space</h2>

								<ul class="fadeInTop">
									<li><span>Strategically located warehouse</span></li>
									<li><span>Easily accessible from the M1</span></li>
									<li><span>Three storey office</span></li>
									<li><span>15m clear internal height</span></li>
									<li><span>55m yard depth</span></li>
								</ul>

							</div>

							<a class="button button--green fadeInTop" target="_blank" href="http://uk.goodman.com/leicester-commercial-park/-/media/Files/Sites/UK%20Logistics/property/properties%20for%20lease/Leicester%20CP/Leicester-335-brochure-Sept17.pdf">Download Brochure</a>

							<a class="button button--grey fadeInTop" href="mailto:nigel.dolan@goodman.com">Get in touch</a>

						</div>

						<div class="col-sm-12 col-md-6">

							<h2 class="title title--medium title--green fadeInTop">Leicester 95</h2>

							<img src="<?php bloginfo('template_url'); ?>/img/leicester-95.jpg" alt="Leicester 95 Image" class="leicester fadeInTop" />

							<img src="<?php bloginfo('template_url'); ?>/img/warehouse.png" alt="Warehous icon" class="warehouse fadeInTop" />

							<div class="warehouse-wrapper">

								<h2 class="title title--large title--green fadeInTop">95,000<span class="title--small">sq ft</span></h2>

								<h2 class="title title--small title--white fadeInTop">High quality, logistics and<br>warehouse space</h2>

								<ul class="fadeInTop">
									<li><span>Strategically located warehouse</span></li>
									<li><span>Immediately adjacent to the M1</span></li>
									<li><span>90 car parking spaces</span></li>
									<li><span>12m clear internal height</span></li>
									<li><span>50m yard depth</span></li>
								</ul>

							</div>

							<a class="button button--green fadeInTop" target="_blank" href="http://uk.goodman.com/leicester-commercial-park/-/media/Files/Sites/UK%20Logistics/property/properties%20for%20lease/Leicester%20CP/Leicester-95-brochure-Sept17.pdf">Download Brochure</a>

							<a class="button button--grey fadeInTop" href="mailto:nigel.dolan@goodman.com">Get in touch</a>

						</div>

					</div>

				</div>

			</section>

		</main><!-- #main -->
	
	</div><!-- #primary -->


	<div class="overlay">
                                
        <div class="overlay-inner">
        
            <h2>Terms &amp; Conditions<span class="title--green">+</span></h2>
            
            <!--<img src="http://www.empiretiles.co.uk/wp-content/themes/super-hijinksified/img/exit.png" class="exit-button" />-->
            <span class="exit-button">+</span>
        
            <div class="disclaimer">
            
                <div class="disclaimer-inner">
                
                    <div class="disclaimer-inner-inner">
                  
						<p>Please note that access to and the use of this is by invite only and subject to the following terms and conditions. References to "we", "us", "our" and "Goodman" and "Goodman Group" refer to Goodman International Limited (ACN 000 123 071) and Goodman Funds Management Limited (ACN 067 796 641; AFSL 223621) as responsible entity for Goodman Industrial Trust, and any of their controlled entities.</p>
						
						<p>1. The promotion and associated competitions are free to enter and are open only to those invited, who are UK Residents and aged 18 years old and over (the "Eligible Participants"). The promotion and associated competitions cannot be extended to any Goodman employees, family, friends or anyone professionally connected with the draw aside from the Eligible Participants.</p>

						<p>2. The prizes cannot be exchanged and no cash alternative is available.</p>
						 
						<p>3. Each Eligible Participant is only allowed to enter the competition once; any double entries will not be counted. No entrant may win more than one prize.</p>

						<p>4. Goodman cannot accept responsibility for entries lost, delayed or mistransmitted. Incomplete entries will be disqualified.</p>

						<p>5. In the event that more than one entrant chooses the correct square, a prize draw will take place to select the main prize winner and three runners up.</p>
						 
						<p>6. The closing date for submissions will be noon on Tuesday 3rd April 2018. Any submissions received after this date will not be counted. The winner will be announced on Tuesday 10th April 2018.
						<p>7. The winner will be notified via an email. The winner must claim their prize within 14 days of Goodman sending notification. If the prize is unclaimed after this time, it will lapse and Goodman reserves the right to offer the unclaimed prize to a substitute winner selected in accordance with these rules.</p>

						<p>8. Goodman reserves the right to feature, and the winner may be required to be featured use of their names and photographs, in Goodman's worldwide publicity and promotional materials.</p>
						 
						<p>9. Goodman reserves the right to change the qualifying criteria and allocation of the prize from time to time at its absolute discretion.</p>
						 
						<p>10. Goodman reserves the right to amend these terms and conditions, and to suspend, terminate or vary the competition prize with or without notice to the Eligible Participants. The mode of notification of suspension, termination or variation shall be at Goodman's sole discretion. No correspondence will be entered into.</p>

						<p>11. Goodman's decision on all matters relating to competition shall be final, binding and conclusive and no correspondence will be entertained.</p>

						<p>12. No purchase is necessary to enter the prize draw.</p>

						<p>13. Goodman will use any data submitted by entrants only for the purposes of running the competition, unless otherwise stated in the entry details. By entering the competition, all entrants consent to the use of their persona; data by Goodman for the purposes of administration of this competition and any other purposes to which the entrant has consented.</p>

						<p>14. By entering the competition each Eligible Participant agrees to be bound by these terms and conditions.</p>

						<p>15. These terms and conditions are governed in accordance with the laws of England and Wales.</p>
                
                	</div>
            
            	</div>
        
            </div>
        
        </div>	
        
    </div>

<?php get_footer(); ?>
