<?php

	if( have_rows('flexible_content') ):

	    while ( have_rows('flexible_content') ) : the_row();

	        if( get_row_layout() == '1_column_text' ):

	        	get_template_part('template-parts/component','1-column-text');

                elseif( get_row_layout() == '2_column_text' ):

                        get_template_part('template-parts/component','2-column-text');

                elseif( get_row_layout() == 'slider' ):

                        get_template_part('template-parts/component','global-slider');

        	elseif( get_row_layout() == 'contact_form' ):

        		get_template_part('template-parts/component','contact');

        	elseif( get_row_layout() == 'image_grid' ):

        		get_template_part('template-parts/component','image-grid');

                elseif( get_row_layout() == 'map' ):

                        get_template_part('template-parts/component','map');

                elseif( get_row_layout() == 'video' ):

                        get_template_part('template-parts/component','video');

                elseif( get_row_layout() == 'accordion_tabs' ):

                        get_template_part('template-parts/component','accordion');

                elseif( get_row_layout() == 'instagram' ):

                        get_template_part('template-parts/component','instagram');

	        endif;

	    endwhile;


	endif;

?>