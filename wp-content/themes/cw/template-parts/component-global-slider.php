<?php if( have_rows('slides') ): ?>

	<div class="global-slider">

		<?php while ( have_rows('slides') ) : the_row(); ?>

			<div class="global-slider__slide well--multiply" style="background-image:url(<?php the_sub_field('slide_image'); ?>);">

				<div class="global-slider__inner">
					
					<div class="row">

						<div class="col-sm-10 float-center fadeInTop">
						
							<h2 class="title title--large title--upper title--push-medium"><?php the_sub_field('slide_title'); ?></h2>

							<p class="title title--xsmall title--upper title--push-large"><?php the_sub_field('slide_subtitle'); ?></p>

							<a href="<?php the_sub_field('slide_link'); ?>" class="button">Learn More</a>

						</div>

					</div>

				</div>
				
			</div>

		<?php endwhile; ?>

	</div>

<?php endif; ?>