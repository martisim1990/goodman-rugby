<div class="section<?php if (get_sub_field('background')) { ?> <?php the_sub_field('background'); ?>"<?php } ?>">

	<div class="container">

		<div class="row">

			<div class="columns col-md-6 fadeInTop">
			
				<?php the_sub_field('left_content'); ?>

			</div>

			<div class="columns col-md-6 fadeInTop">
			
				<?php the_sub_field('right_content'); ?>

			</div>

		</div>

	</div>

</div>