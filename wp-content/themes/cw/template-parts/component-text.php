<div class="section<?php if (get_sub_field('background')) { ?> <?php the_sub_field('background'); ?><?php } ?>">

	<div class="container">

		<div class="row">

			<div class="columns col-md-8 fadeInTop">
			
				<?php the_sub_field('content'); ?>

			</div>

		</div>

	</div>

</div>