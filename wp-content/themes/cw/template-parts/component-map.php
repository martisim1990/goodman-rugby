<div class="section<?php if (get_sub_field('background')) { ?> <?php the_sub_field('background'); ?><?php } ?>">

	<?php the_sub_field('map', 'options'); ?>

</div>