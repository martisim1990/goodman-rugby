<div class="section<?php if (get_sub_field('background')) { ?> <?php the_sub_field('background'); ?><?php } ?>">

	<div class="container">

		<div class="row">

			<?php if (get_sub_field('title')) { ?>

				<div class="col-sm-12 col-lg-9 float-center fadeInTop">

					<h2 class="title title--center title--push-large"><?php the_sub_field('title'); ?></h2>

				</div>

			<?php } ?>

			<?php

				if( have_rows('image_grid') ):

			    	while ( have_rows('image_grid') ) : the_row(); 
						
						?>

							<div class="columns column-block col-sm-12 col-md-2 col-lg-4 fadeInTop">
								
								<a href="<?php the_sub_field('image'); ?>" class="image-link">
								
									<div class="ratio ratio--square background-image" style="background-image:url(<?php the_sub_field('image'); ?>);"></div>

								</a>

							</div>

						<?php 
							
					endwhile;

				endif;
			?>

		</div>

	</div>

</div>