<div class="section<?php if (get_sub_field('background')) { ?> <?php the_sub_field('background'); ?><?php } ?>">

	<div class="container">

		<div class="row">

			<div class="col-md-8 col-md-offset-2 fadeInTop">

				<div class="videoWrapper">

					<?php the_sub_field('video'); ?>

				</div>

			</div>

		</div>

	</div>

</div>