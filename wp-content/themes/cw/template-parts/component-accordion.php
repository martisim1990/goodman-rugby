<div class="section">
 
    <div class="container">

        <div class="row">
            
            <!-- Tab Desktop Begin -->
            
            <div class="columns col-sm-12 fadeInTop visible-md visible-lg">    

                <?php if( have_rows('accordion') ): $i=0; ?>

                    <ul id="myTabs" class="tab-list" role="tablist">

                        <?php while ( have_rows('accordion') ) : the_row(); $i++; ?>
                    
                            <li class="tab-item <?php if ($i == 1) {?>active<?php } ?>">
                                
                                <a href="#accordion-<?php echo $i;?>" data-toggle="tab">

                                    <?php the_sub_field('title'); ?>

                                </a>

                            </li>

                        <?php endwhile; ?>
                        
                    </ul>

                <?php endif; ?>

                <?php if (have_rows('accordion') ): $i=0; ?>

                    <div class="tab-content">
            
                        <?php while ( have_rows('accordion') ) : the_row(); $i++; ?>

                            <div role="tabpanel" class="tab-pane <?php if ($i == 1) {?>active<?php } ?>" id="accordion-<?php echo $i;?>">
                                    
                                <?php the_sub_field('content'); ?>
                            
                            </div>

                        <?php endwhile; ?>
                    
                    </div>
                
                <?php endif; ?>

            </div>
            
            <!-- Tab Desktop End -->

            <!-- Accordion Mobile Begin -->

            <div class="columns col-sm-12 fadeInTop hidden-md hidden-lg">

                <?php if(have_rows('accordion') ): $i=0; ?>
                    
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

                        <?php while ( have_rows('accordion') ) : the_row(); $i++; ?>

                            <div class="panel panel-default">
                                
                                <div class="panel-heading" role="tab" id="heading<?php echo $i; ?>">
                                
                                    <h4 class="panel-title">
                                        
                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $i; ?>" aria-expanded="true" aria-controls="collapse<?php echo $i; ?>">
                                            
                                            <?php the_sub_field('title'); ?>

                                        </a>
                                    
                                    </h4>
                                
                                </div>
                                
                                <div id="collapse<?php echo $i; ?>" class="panel-collapse collapse <?php if ($i == 1) {?>in<?php } ?>" role="tabpanel" aria-labelledby="heading<?php echo $i; ?>">
                                
                                    <div class="panel-body">
                                
                                        <?php the_sub_field('content'); ?>
                                
                                    </div>
                                
                                </div>

                            </div>
                            
                        <?php endwhile; ?>
                
                    </div>

                <?php endif; ?>

            </div>
        
            <!-- Accordion Mobile End -->
        
        </div>

    </div>

</div>
