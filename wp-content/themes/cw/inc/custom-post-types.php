<?php 
/**
 * talyllyn Custom Post Types
 *
 * @package talyllyn
 */

register_post_type( 'case-studies', array(
  'labels'            => array(
                        'name'          => __( 'Case Studies' ),
                        'singular_name' => __( 'Case Study' )
                      ),
  'has_archive'       => false,
  'public'            => true,
  'hierarchical'      => true,
  'supports'          => array( 'title', 'excerpt'),
  'show_in_nav_menus' => true,
  'menu_icon'         => 'dashicons-editor-quote',
  'rewrite'           => array( 'slug' => 'case-studies'),
));

?>