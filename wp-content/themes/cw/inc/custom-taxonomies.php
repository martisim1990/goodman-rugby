<?php  

function tr_create_my_taxonomy() {

    register_taxonomy(
        'case-study-category',
        'case-studies',
        array(
            'label' => __( 'Case Study Category' ),
            'rewrite' => array( 'slug' => 'events-category' ),
            'hierarchical' => true,
        )
    );
}
add_action( 'init', 'tr_create_my_taxonomy' );

?>